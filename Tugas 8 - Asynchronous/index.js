// setTimeout(() => {
// 	console.log("Saya dijalankan 2");

// 	setTimeout(() => {
// 		console.log("Saya dijalankan 3");

// 		setTimeout(() => {
// 			console.log("Saya dijalankan 4");
// 		}, 3000);
// 	}, 3000);
// }, 3000);

// console.log("Saya dijalankan pertama");

// let timer = 0;

// setInterval(() => {
// 	console.log(`Sekarang sudah ${++timer}`);
// }, 1000);

// function periksaDoketer(nomorAntri, callback) {
// 	setTimeout(() => {
// 		if (nomorAntri > 50) {
// 			callback(false);
// 		} else if (nomorAntri < 10) {
// 			callback(true);
// 		}
// 	}, 3000);
// }

// periksaDoketer(65, (check) => {
// 	if (check) {
// 		console.log("Sebentar lagi giliran saya");
// 	} else {
// 		console.log("Saya jalan-jalan dulu");
// 	}
// });

// const willIGetNewPhone = (isMomhappy) =>
// 	new Promise((resolve, reject) => {
// 		setTimeout(() => {
// 			if (isMomhappy) {
// 				const phone = {
// 					brand: "Samsung",
// 					color: "Black",
// 				};

// 				resolve({ phone, num: 100 });
// 			} else {
// 				const reason = new Error("Mom is not happy");

// 				reject(reason);
// 			}
// 		}, 1000);
// 	});

// willIGetNewPhone(true)
// 	.then((phone) => {
// 		console.log("Fullfilled", phone);
// 	})
// 	.catch((error) => {
// 		console.error(error);
// 	});

// Ambil Data Pesan
// Profil Orang yang kirim
// Foto profil
// Ambil video message

// willIGetNewPhone(true)
// 	.then((phone) => {
// 		console.log("Data pesan diterima");
// 		willIGetNewPhone(true)
// 			.then((phone) => {
// 				console.log("Data profile pengirim ditermia");
// 				willIGetNewPhone(true)
// 					.then((phone) => {
// 						console.log("Data foto pengirim ditermia");
// 						willIGetNewPhone(true)
// 							.then((phone) => {
// 								console.log("Data pesan video ditermia");
// 							})
// 							.catch((error) => {
// 								console.error(error);
// 							});
// 					})
// 					.catch((error) => {
// 						console.error(error);
// 					});
// 			})
// 			.catch((error) => {
// 				console.error(error);
// 			});
// 	})
// 	.catch((error) => {
// 		console.error(error);
// 	});

// const ambilData = (namaData, isResolved) =>
// 	new Promise((resolve, reject) => {
// 		console.log(`Mengambil data ${namaData}`);
// 		setTimeout(() => {
// 			if (isResolved) {
// 				const data = `${namaData} berhasil diambil`;

// 				resolve(data);
// 			} else {
// 				const reason = `${namaData} gagal diambil`;

// 				reject(reason);
// 			}
// 		}, 3000);
// 	});

// ambilData("Pesan", false)
// 	.then((data) => {
// 		console.log(data);
// 	})
// 	.catch((err) => {
// 		console.log(err);
// 	});

// const runMe = async () => {
// 	try {
// 		let res;

// 		res = await ambilData("Pesan", true);
// 		console.log(res);

// 		res = await ambilData("Foto Profil", false);
// 		console.log(res);

// 		res = await ambilData("Pesan Video", true);
// 		console.log(res);
// 	} catch (error) {
// 		console.error(error);
// 	}
// };

// runMe();

const readBooks = require("./callback.js");

const books = [
	{ name: "LOTR", timeSpent: 3000 },
	{ name: "Fidas", timeSpent: 2000 },
	{ name: "Kalkulus", timeSpent: 4000 },
];

readBooks(5000, books[0], () => {
	console.log("Sudah selesai baca");
});
