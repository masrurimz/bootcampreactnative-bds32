// var flag = 20;
// while (flag > 1) {
// 	console.log("Iterasi while ke-", flag);
// 	flag -= 3;
// }

// for (var key = 1; key < 10; key += 2) {
// 	console.log("Iterasi for ke-", key);
// }

// for (var key = 1; key < 21; key += 1) {
// 	var isFiveMultiply = key % 5 === 0;
// 	var isThreeMultiply = key % 3 === 0;

// 	if (isFiveMultiply || isThreeMultiply) {
// 		continue;
// 	}

// 	console.log("Iterasi for ke-", key);
// }

// var searchCharacter = "g";
// var kalimat = "Dang ding dung dang dung";
// for (var i = 0; i < kalimat.length; i++) {
// 	if (kalimat[i] === "g") {
// 		console.log("Index dari", searchCharacter, "adalah", i);
// 		break;
// 	}
// }

var kalimat = "Dang ding dung dang dung";
// for (var i = 0; i < 5; i++) {
// 	console.log("Kalimat index", i, kalimat[i]);
// }

var buffer = "";
for (var i = 0; i < kalimat.length; i++) {
	// for (var character of kalimat) {
	// buffer += character;
	// console.log(character);
	var character = kalimat[i];

	var isSpace = character === " ";
	buffer += isSpace ? "\n" : character;
}

console.log(buffer);
