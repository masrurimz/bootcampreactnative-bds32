import React, { useEffect, useState } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { useSelector } from "react-redux";
import { getAuth, onAuthStateChanged } from "firebase/auth";

import "react-native-gesture-handler";
import Login from "./Login";
import Home from "./Home";
import Register from "./Register";
import { useDispatch } from "react-redux";
import { setSignInStatus } from "./redux/authSlice";

const Stack = createStackNavigator();

export default function index() {
	const dispatch = useDispatch();

	const isSignedIn = useSelector((state) => state.auth.isSignIn);
	// const [isSignedIn, setIsSignedIn] = useState(false);

	useEffect(() => {
		const auth = getAuth();
		const unsubscribe = onAuthStateChanged(auth, (user) => {
			if (user) {
				// User is signed in, see docs for a list of available properties
				// https://firebase.google.com/docs/reference/js/firebase.User
				const uid = user.uid;
				dispatch(setSignInStatus(true));
				// setIsSignedIn(true);
				// ...
			} else {
				// User is signed out
				// ...
				dispatch(setSignInStatus(false));

				// setIsSignedIn(false);
			}
		});

		return unsubscribe;
	}, []);
	// const token = useSelector((state) => state.auth.token);
	// const token = "";

	return (
		<NavigationContainer>
			<Stack.Navigator initialRouteName="Login">
				{!isSignedIn ? (
					<>
						<Stack.Screen
							name="Login"
							component={Login}
							options={{ headerShown: false }}
						/>
						<Stack.Screen
							name="Register"
							component={Register}
							options={{ headerShown: false }}
						/>
					</>
				) : (
					<Stack.Screen
						name="Home"
						component={Home}
						options={{ headerTitle: "Daftar Barang" }}
					/>
				)}
			</Stack.Navigator>
		</NavigationContainer>
	);
}
