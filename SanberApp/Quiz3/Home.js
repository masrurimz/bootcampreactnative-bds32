import { DrawerItem } from "@react-navigation/drawer";
import React, { useCallback, useEffect } from "react";
import { useState } from "react";
import { StyleSheet, Text, View, Image, Button } from "react-native";
import { FlatList } from "react-native-gesture-handler";
import { interpolate } from "react-native-reanimated";
import { useSelector } from "react-redux";
import { getAuth, signOut } from "firebase/auth";
import {
	collection,
	addDoc,
	getFirestore,
	getDoc,
	getDocs,
	query,
	where,
} from "firebase/firestore";

import { Data } from "./data";
import { useFocusEffect } from "@react-navigation/native";
export default function Home({ route, navigation }) {
	const username = useSelector((state) => state.auth.userName);
	// const username = "";

	// const { username } = route.params;
	const [totalPrice, setTotalPrice] = useState(0);

	const currencyFormat = (num) => {
		return "Rp " + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
	};

	useFocusEffect(
		useCallback(() => {
			const fetchCart = async () => {
				const auth = getAuth();
				const uid = auth.currentUser.uid;

				const db = getFirestore();
				const cartRef = collection(db, "cart");

				// Create a query against the collection.
				const q = query(cartRef, where("user_uid", "==", uid));

				const querySnapshot = await getDocs(q);

				let dbTotalPrice = 0;

				querySnapshot.forEach((item) => {
					dbTotalPrice += item.data().harga;
				});

				console.log({ dbTotalPrice });

				setTotalPrice(dbTotalPrice);
			};

			fetchCart();
		}, []),
	);

	const updateHarga = async (price, item) => {
		try {
			const auth = getAuth();
			const uid = auth.currentUser.uid;

			const db = getFirestore();
			const docRef = await addDoc(collection(db, "cart"), {
				...item,
				user_uid: uid,
			});
			console.log("Document written with ID: ", docRef.id);

			console.log("UpdatPrice : " + price);
			const temp = Number(price) + totalPrice;
			console.log(temp);
			setTotalPrice(temp);
		} catch (e) {
			console.error("Error adding document: ", e);
		}
	};
	useEffect(() => {
		console.log("yang dicetak params:", username);
	}, []);

	return (
		<View style={styles.container}>
			<View
				style={{
					flexDirection: "row",
					justifyContent: "space-between",
					padding: 16,
				}}>
				<View>
					<Text>Selamat Datang,</Text>
					<Text style={{ fontSize: 18, fontWeight: "bold" }}>{username}</Text>
				</View>
				<View>
					<Text>Total Harga:</Text>
					<Text style={{ fontSize: 18, fontWeight: "bold" }}>
						{" "}
						{currencyFormat(totalPrice)}
					</Text>
					<Button
						title="Sign Out"
						onPress={() => {
							const auth = getAuth();

							signOut(auth);
						}}
					/>
				</View>
			</View>
			<View
				style={{ alignItems: "center", marginBottom: 20, paddingBottom: 60 }}>
				<FlatList
					data={Data}
					numColumns={2}
					keyExtractor={(item) => item.id}
					showsVerticalScrollIndicator={false}
					renderItem={({ item }) => {
						return (
							<View style={styles.content}>
								<Text>{item.title}</Text>
								<Image
									style={{ width: 120, height: 100, alignSelf: "center" }}
									source={item.image}
								/>
								<Text>{item.harga}</Text>
								<Text>{item.type} </Text>
								<Button
									onPress={() => updateHarga(item.harga, item)}
									title="Beli"
								/>
							</View>
						);
					}}
				/>
			</View>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "white",
	},
	content: {
		width: 150,
		height: 220,
		margin: 5,
		borderWidth: 1,
		alignItems: "center",
		borderRadius: 5,
		borderColor: "grey",
	},
});
