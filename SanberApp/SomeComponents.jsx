import { View, Text } from "react-native";
import React from "react";

export const Dog = () => {
	return (
		<View>
			<Text>Dog</Text>
		</View>
	);
};

export const Horse = () => {
	return (
		<View>
			<Text>Horse</Text>
		</View>
	);
};

export const Duck = () => {
	return (
		<View>
			<Text>Duck</Text>
		</View>
	);
};
