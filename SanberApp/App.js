import { StatusBar } from "expo-status-bar";
import { useState } from "react";
import { StyleSheet, Text, TextInput, View } from "react-native";
import { getApps, initializeApp } from "firebase/app";

import Tugas12 from "./Tugas/Tugas12";
import Cat from "./Cat";
import { Dog, Duck, Horse } from "./SomeComponents";
import Flexbox from "./Tugas/Tugas13/FlexBox";
import Tugas13 from "./Tugas/Tugas13";
import Telegram from "./Tugas/Tugas12/Telegram";
import Props from "./Tugas/Tugas13/Props";
import Tugas14 from "./Tugas/Tugas14";
import Tugas15 from "./Tugas/Tugas15";
import RestApi from "./Tugas/Tugas14/RestApi";
import Quiz3 from "./Quiz3";

import "react-native-gesture-handler";
import Tugas16 from "./Tugas/Tugas16";
import Tugas17 from "./Tugas/Tugas17";

import { Provider } from "react-redux";
import { store } from "./Quiz3/redux/store";

const firebaseConfig = {
	apiKey: "AIzaSyBA338DFwpg6QW-qWa7NEUvtn5D_ruITFE",
	authDomain: "sanberapps-bds32.firebaseapp.com",
	projectId: "sanberapps-bds32",
	storageBucket: "sanberapps-bds32.appspot.com",
	messagingSenderId: "264698912484",
	appId: "1:264698912484:web:2e8c25d2dc28c966a996f1",
};

// Initialize Firebase
if (!getApps().length) {
	const app = initializeApp(firebaseConfig);
}

// console.log(getApps());

export default function App() {
	// const greetings = "Hello World";
	// const [isChecked, setIsChecked] = useState(false);
	// const [name, setName] = useState("");

	return (
		// <View style={styles.container}>
		// 	<Text>{name}</Text>
		// 	<TextInput value={name} onChangeText={setName} />
		// 	<Text>Open up App.js to start working on your app!</Text>
		// 	<Text>{greetings}</Text>
		// 	<Cat />
		// 	<Dog />
		// 	<Duck />
		// 	<Horse />
		// </View>
		// <Provider store={store}>
		// 	<StatusBar style="light" translucent={false} />
		// 	{/* <Props /> */}
		// 	{/* <Telegram /> */}
		// 	{/* <Tugas12 /> */}
		// 	{/* <Flexbox /> */}
		// 	{/* <Tugas13 /> */}
		// 	{/* <Tugas14 /> */}
		// 	{/* <Tugas15 /> */}
		// 	{/* <RestApi /> */}
		// 	<Quiz3 />
		// 	{/* <Tugas16 /> */}
		// 	{/* <Tugas17 /> */}
		// </Provider>
		<Tugas15 />
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#fff",
		alignItems: "center",
		justifyContent: "center",
	},
});
