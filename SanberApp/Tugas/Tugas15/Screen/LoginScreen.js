import React from "react";
import { StyleSheet, Text, View, Button } from "react-native";

export default function Login({ navigation }) {
	return (
		<View style={styles.container}>
			<Text>Halaman login</Text>
			<Button
				onPress={() =>
					navigation.navigate("MyDrawer", {
						screen: "App",
						params: {
							screen: "AboutScreen",
						},
					})
				}
				title="menuju halaman homescreen"></Button>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
	},
});
