import React from "react";
import { Text } from "react-native";

const Cat = () => {
	return <Text>This is a Cat</Text>;
};

export const Dog = () => {
	return <Text>This is a Dog</Text>;
};

export const Fish = () => {
	return <Text>This is a Fish</Text>;
};

export default Cat;

// export { Dog, Fish };
