import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
const Card = ({ title, movieLength }) => {
	return (
		<TouchableOpacity style={styles.Button2}>
			<Text>{title}</Text>
			<Text>{movieLength}</Text>
		</TouchableOpacity>
	);
};

export default function Props() {
	return (
		<View style={styles.container}>
			<Card title="monkey" movieLength={100} />
			<Card title="One Piece" />
			<Card title="Naruto" />
			<Card title="Saitama" />
			<Card title="Dragon ball" />
			<Card title="Boku No Hero" />
		</View>
	);
}

const styles = StyleSheet.create({
	Button2: {
		width: 300,
		padding: 10,
		backgroundColor: "skyblue",
		justifyContent: "center",
		alignItems: "center",
		borderWidth: 1,
		margin: 5,
	},
	container: {
		padding: 16,
	},
});
