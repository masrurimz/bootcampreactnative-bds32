import {
	StyleSheet,
	Text,
	TextInput,
	TouchableOpacity,
	View,
} from "react-native";
import React from "react";
import { MaterialCommunityIcons } from "@expo/vector-icons";

const FormTextInput = ({
	title,
	secureTextEntry = false,
	value = "",
	onChangeText = (text = "") => {},
}) => {
	return (
		<View style={styles.form}>
			<Text style={styles.formTitle}>{title}</Text>
			<View style={styles.formFieldText}>
				<TextInput
					style={styles.formFieldTextInput}
					secureTextEntry={secureTextEntry}
					value={value}
					onChangeText={onChangeText}
				/>
				{secureTextEntry ? (
					<TouchableOpacity>
						<MaterialCommunityIcons
							name="eye-off-outline"
							size={24}
							color="black"
						/>
					</TouchableOpacity>
				) : null}
			</View>
		</View>
	);
};

export default FormTextInput;

const styles = StyleSheet.create({
	form: {
		marginBottom: 20,
	},
	formTitle: {
		fontWeight: "bold",
		color: "grey",
		fontSize: 16,
		marginBottom: 5,
	},
	formFieldText: {
		borderBottomWidth: 1,
		paddingBottom: 5,
		flexDirection: "row",
	},
	formFieldTextInput: {
		fontSize: 16,
		color: "black",
		flex: 1,
	},
});
