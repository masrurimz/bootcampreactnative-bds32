import {
	View,
	Text,
	StyleSheet,
	TextInput,
	Image,
	TouchableOpacity,
} from "react-native";
import React from "react";
import {
	useFonts,
	Montserrat_900Black_Italic,
} from "@expo-google-fonts/montserrat";

import Props from "./Props";
import FormTextInput from "./Components/FormTextInput";
import Button from "./Components/Button";

const Tugas13 = () => {
	const [fontsLoaded] = useFonts({
		Montserrat_900Black_Italic,
		"Inter-ExtraLight": require("./Assets/Fonts/Inter-ExtraLight.ttf"),
	});

	if (!fontsLoaded) {
		return null;
	}

	return (
		<View style={styles.container}>
			<Image source={require("../../assets/Logo.png")} style={styles.header} />
			<Text style={styles.title}>Daftar</Text>
			<View style={styles.form}>
				<FormTextInput title={"Username"} />
				<FormTextInput title={"Email"} />
				<FormTextInput title={"Password"} secureTextEntry />
				<FormTextInput title={"Ulangi Password"} secureTextEntry />
			</View>
			<Button title={"Daftar"} />
		</View>
	);
};

export default Tugas13;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: 20,
	},
	header: {
		alignSelf: "center",
		marginBottom: 40,
	},
	title: {
		fontSize: 32,
		// fontWeight: "bold",
		marginBottom: 40,
		// fontFamily: "Montserrat_900Black_Italic",
		fontFamily: "Inter-ExtraLight",
	},
	form: {
		flex: 1,
	},
});
