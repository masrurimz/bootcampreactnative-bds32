import client from "./client";

export const getNews = () => client.get("/news");

export const createNews = ({ title, value }) =>
	client.post("/news", {
		title,
		value,
	});

export const updateNews = ({ id, title, value }) =>
	client.put(`/news/${id}`, {
		title,
		value,
	});

export const deleteNews = ({ id }) => client.delete(`/news/${id}`);
