import axios from "axios";

const client = axios.create({
	baseURL: "https://sanbers-news-api.herokuapp.com/api",
});

export default client;
