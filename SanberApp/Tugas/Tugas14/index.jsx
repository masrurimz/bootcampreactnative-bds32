import React, { useEffect, useState } from "react";
import { Button, View, Text, TextInput } from "react-native";
import FormTextInput from "../Tugas13/Components/FormTextInput";
import { useCountdown } from "./Hooks";

const App = () => {
	// Deklarasi variabel state baru yang kita sebut "count"
	const [count, setCount] = useState(10);

	const { timer } = useCountdown(500, 1000);

	useEffect(() => {
		console.log("Count", count);
	}, [count]);

	// DidMount
	useEffect(() => {
		console.log("Dijalankan Sekali");
	}, []);

	// const [nama, setNama] = useState("");

	// console.log({ nama });

	const [form, setForm] = useState({
		firstName: "Santai",
		lastName: "Berkualitas",
	});

	// console.log(form, "form");

	return (
		<View style={{ justifyContent: "center", flex: 1, paddingHorizontal: 16 }}>
			<Button onPress={() => setCount((value) => value + 50)} title="Tambah" />
			<Text style={{ alignItems: "center" }}>
				Anda menekan sebanyak {count} kali
			</Text>
			<Button onPress={() => setCount((value) => value - 1)} title="Kurang" />
			<Button onPress={() => setCount(100)} title="Reset ke 100" />

			{/* <TextInput value={nama} onChangeText={setNama} /> */}

			<FormTextInput title={"Timer"} value={timer.toString()} />

			<FormTextInput
				title={"FistName"}
				value={form.firstName}
				onChangeText={(firstName) => {
					setForm((prevValue) => ({
						...prevValue,
						firstName,
					}));
				}}
			/>
			<FormTextInput
				title={"LastName"}
				value={form.lastName}
				onChangeText={(lastName) => {
					setForm((prevValue) => ({
						...prevValue,
						lastName,
					}));
				}}
			/>
		</View>
	);
};

export default App;
