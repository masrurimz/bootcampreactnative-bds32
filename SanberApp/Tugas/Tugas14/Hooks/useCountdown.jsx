import { useEffect, useState } from "react";

export const useCountdown = (initialCount = 0, interval = 1000) => {
	const [timer, setTimer] = useState(initialCount);

	useEffect(() => {
		const timer = setInterval(() => {
			setTimer((prevTimer) => --prevTimer);
		}, interval);

		return () => {
			clearInterval(timer);
		};
	}, []);

	return {
		timer,
	};
};
