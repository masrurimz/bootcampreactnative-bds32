// class Vechile {
// 	constructor(carName) {
// 		this._name = carName;
// 		this.wheel = 4;
// 		this.isDiesel = true;
// 		// console.log("Class di instance");
// 	}

// 	present(x) {
// 		// console.log("carName", carName);
// 		return `${x}, I have a ${this.name}`;
// 	}

// 	static hello() {
// 		return "Hello!!";
// 	}
// }

// // const car = new Vechile("Ford");
// // console.log(car);
// // console.log(car.present("Hello"));
// // console.log(Vechile.hello());
// // console.log(car.hello());

// class Car extends Vechile {
// 	constructor(name) {
// 		super(name);
// 		this.isDiesel = false;
// 	}

// 	show() {
// 		return `${this.present("Hi")}, it has ${this.wheel} wheels`;
// 	}

// 	get name() {
// 		return this._name.toUpperCase();
// 	}

// 	set name(x) {
// 		this._name = `${x[0].toUpperCase()}${x.slice(1).toLowerCase()}`;
// 	}
// }

// const mcQuen = new Car("Ferrari");
// console.log(mcQuen);
// // console.log(mcQuen.present("Hi"));
// console.log(mcQuen.show());
// console.log(mcQuen.name);

// mcQuen.name = "lamBoRGhini";
// console.log(mcQuen);
// console.log(mcQuen.name);

function Clock({ template }) {
	var timer;

	function render() {
		var date = new Date();

		var hours = date.getHours();
		if (hours < 10) hours = "0" + hours;

		var mins = date.getMinutes();
		if (mins < 10) mins = "0" + mins;

		var secs = date.getSeconds();
		if (secs < 10) secs = "0" + secs;

		var output = template
			.replace("h", hours)
			.replace("m", mins)
			.replace("s", secs);

		console.log(output);
	}

	this.stop = function () {
		clearInterval(timer);
	};

	this.start = function () {
		render();
		timer = setInterval(render, 1000);
	};
}

var clock = new Clock({ template: "h:m:s" });
clock.start();
