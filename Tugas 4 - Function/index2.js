console.log("Tugas No. 1 Looping While");

console.log("LOOPING PERTAMA\n");
var a = 2;
while (a <= 20) {
	if (a % 2 == 0) {
		console.log(a + " - I love coding");
	}
	a++;
}
console.log("\n");

console.log("LOOPING KEDUA\n");
var b = 20;
while (b >= 2) {
	if (b % 2 == 0) {
		console.log(b + " - I will become a mobile developer");
	}
	b--;
}
console.log("\n");

console.log("Tugas No. 2 Looping For");
var k;
for (k = 1; k <= 20; k++) {
	if (k % 3 == 0 && k % 2 != 0) {
		console.log(k + " - I love Coding");
	} else if (k % 2 == 0) {
		console.log(k + " - Berkualitas");
	} else {
		console.log(k + " - Santai");
	}
}
console.log("\n");
// var i;
// for (i=1; i<=20 ;i++) {
// if(i%3==0){
// console.log("angka = " + i + "<br>");
// }
// }

console.log("Tugas Nomor 3\n");
var input = 4;
var kali = "########";
var keluar = "";

for (let i = 0; i < input; i++) {
	for (let j = 0; j < 1; j++) {
		keluar = kali;
	}
	console.log(keluar);
}

console.log("Tugas Nomor 4\n");

var loop = 7;
var output = "";
var bintang = "#";
for (let i = 0; i < loop; i++) {
	for (let j = 0; j < 1; j++) {
		output += bintang;
	}
	console.log(output);
}

console.log("Soal Nomor 5\n");
var kali1 = " ####\n####";
var hasil = "";
var loop2 = 4;
for (let i = 0; i < loop2; i++) {
	for (let j = 0; j < 1; j++) {
		hasil = kali1;
	}
	console.log(hasil);
}
