// function tampilkan() {
// 	console.log("halo!");
// }

// tampilkan();
// tampilkan();
// tampilkan();
// tampilkan();

// function munculkanAngkaDua() {
// 	// return console.log("100");
// 	// return 10;
// }

// var hasil = munculkanAngkaDua();
// console.log(hasil);
// console.log(console.log(1000));
// console.log(undefenined);

// function tampilkan() {
// 	return "halo!";
// 	// return console.log("halo!");

// 	// return 10;
// }

// var hasil = tampilkan();
// console.log(hasil);

// function jumlakan(a, b) {
// 	console.log("nilai b", b);

// 	return a + b;
// }

// var hasil = jumlakan(10);
// console.log(hasil);

// function kalikan(a, b, c = 1) {
// 	// console.log("nilai b", b);
// 	// console.log({ a, b, c });
// 	console.log(10, 1, "2", 3, "4", true);

// 	return a * b * c;
// }

// var hasil = kalikan(10);
// console.log(hasil);

// // Function as first class object
// function add(a, b) {
// 	return a + b;
// }
// console.log(add(10, 7));

// var tambahkan = add;
// console.log(tambahkan(10, 7));

// var kalikan = function (a, b, c) {
// 	return a * b * c;
// };
// console.log(kalikan(7, 10));

// var operasikan = function (x, y, operasi) {
// 	var hasil = operasi(x, y);

// 	return "Hasil operasi " + x + " dengan " + y + " adalah " + hasil;
// };
// console.log(operasikan(10, 10, kalikan));
// console.log(operasikan(10, 10, add));

// function tampilkan() {
// 	return console.log("halo!");
// }

// var returnValue = tampilkan();
// console.log("returnValue", returnValue);

// Pure Funcion

// var hasil = 0;
// var a = 17;
// var b = 5;

// function jumlahkan() {
// 	hasil = a + b;
// }

// function jumlahkan(a, b) {
// 	return a + b;
// }

// console.log(jumlahkan(17, 5));
// // console.log();

// function buatTangga(tinggi) {
// 	var buffer = "";

// 	for (var row = 0; row <= tinggi; row++) {
// 		for (var col = 0; col < row; col++) {
// 			buffer += "#";
// 		}
// 		buffer += "\n";
// 	}

// 	return buffer;
// }

// var tangga3 = buatTangga(3);
// console.log(tangga3);

// var tangga7 = buatTangga(7);
// console.log(tangga7);

// var tangga5 = buatTangga(5);
// console.log(tangga5);

// function convertDate(tanggal, bulan, tahun) {
// 	var bulanStr = "";

// 	switch (bulan) {
// 		case 1: {
// 			bulanStr = "Januari";
// 			break;
// 		}
// 		case 2: {
// 			bulanStr = "Februari";
// 			break;
// 		}
// 		case 3: {
// 			bulanStr = "Maret";
// 			break;
// 		}
// 		case 4: {
// 			bulanStr = "April";
// 			break;
// 		}
// 		case 5: {
// 			bulanStr = "Mei";
// 			break;
// 		}
// 		case 6: {
// 			bulanStr = "Juni";
// 			break;
// 		}
// 		case 7: {
// 			bulanStr = "Juli";
// 			break;
// 		}
// 		case 8: {
// 			bulanStr = "Agustus";
// 			break;
// 		}
// 		case 9: {
// 			bulanStr = "September";
// 			break;
// 		}
// 		case 10: {
// 			bulanStr = "Oktober";
// 			break;
// 		}
// 		case 11: {
// 			bulanStr = "November";
// 			break;
// 		}
// 		case 12: {
// 			bulanStr = "Desember";
// 			break;
// 		}
// 		default: {
// 			flag = false;
// 		}
// 	}

// 	return tanggal + " " + bulanStr + " " + tahun;
// }

// var newDate = convertDate(20, 5, 2022);
// console.log(newDate);

// var newDate2 = convertDate(7, 6, 1990);
// console.log(newDate2);

// var newDate3 = convertDate(7, 15, 1990);
// console.log(newDate3);

function jumlahkanBiasa(a, b) {
	return a + b;
}

var jumlahkanAnonym = function (a, b) {
	return a + b;
};

var jumlahkanArrow = (a, b) => {
	return a + b;
};

// Function as first class object
function add(a, b) {
	return a + b;
}
console.log(add(10, 7));

var tambahkan = add;
console.log(tambahkan(10, 7));

var kalikan = function (a, b, c) {
	return a * b * c;
};
console.log(kalikan(7, 10));

var operasikan = function (x, y, operasi) {
	var hasil = operasi(x, y);

	return "Hasil operasi " + x + " dengan " + y + " adalah " + hasil;
};
console.log(operasikan(10, 10, kalikan));
console.log(operasikan(10, 10, add));
console.log(
	operasikan(10, 7, function (x, y) {
		return x - y;
	}),
);
console.log(
	operasikan(10, 7, (x, y) => {
		return x - y;
	}),
);
