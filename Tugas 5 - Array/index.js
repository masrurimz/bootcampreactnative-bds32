// var hobbies = ["coding", "cycling", "climbing", "skateboarding"];

// // console.log(hobbies[hobbies.length - 1]);
// // console.log(hobbies.slice(-1));
// hobbies.push("surfing", "hiking");
// console.log(hobbies);

// var numbers = [0, 1, 2, 3];
// numbers.unshift(-1);
// console.log(numbers);

// numbers.unshift(-3, -2);
// console.log(numbers);

// var removedElement = numbers.shift();
// numbers.shift();
// numbers.shift();
// console.log(numbers);

// console.log(removedElement);

// var animals = ["Kuda", "kera", "gajah", "musang"];
// animals.sort();
// console.log(animals); // ["gajah", "kera", "musang"]

// var animals = ["Kuda", "kera", "gajah", "musang"];
// animals.sort().reverse();
// console.log(animals); // ["gajah", "kera", "musang"]

// var numbers = [12, 1, 3];
// numbers.sort();
// console.log(numbers); // [1, 12, 3]

// var numbers = [12, 1, 3];
// // Mengurutkan secara descending
// numbers.sort(function (a, b) {
// 	return b - a;
// });
// console.log(numbers); // [1, 3, 12]

// var angka = [0, 1, 2, 3, 4, 5, 6];
// var irisan1 = angka.slice(1, -3);
// console.log(irisan1);

// var fruits = ["banana", 100, 101, "orange", true, "grape"];
// // fruits.splice(1, 2, "wartermellon", [10, 21]);

// fruits.sort();
// console.log(fruits);

// var kalimat = "clean exit - waiting for changes before restart";
// console.log(kalimat.split(" "));
// console.log(kalimat);

// var arrayMulti = [
// 	[1, 2, 3],
// 	[4, 5],
// 	[6, [7, 8]],
// ];

// console.log(arrayMulti);
// console.log(arrayMulti[0][0]);
// console.log(arrayMulti[2][1]);
// console.log(arrayMulti[1][1][1]?.[0]);

// var arrayMulti = [
// 	[1, 2, 3],
// 	[4, 5, 6],
// 	[7, 8, 9],
// ];

// arrayMulti.forEach(function (value, index, array) {
// 	console.log(index, value);
// });

// var angka = [1, 2, 3, 4];
// var resultMap = angka.map(function (value) {
// 	var isEven = value % 2 === 0;

// 	if (isEven) {
// 		return value * 10;
// 	}

// 	return value * 4;
// });
// console.log(resultMap);

// var angka = [1, 2, 3, 4, 5, 6];
// var resultFilter = angka.filter(function (value) {
// 	var isOdd = value % 2 !== 0;

// 	return isOdd;
// });
// console.log(resultFilter);

// var angka = [1, 2, 3, 4, 5, 6];
// var sum = angka.reduce(function (sum, value) {
// 	return sum + value;
// }, 0);
// console.log(sum);

// [absen, nama, nilai]
// var dataSiswa = [
// 	[10, "John", 90],
// 	[30, "Erik", 70],
// 	[5, "Aton", 70],
// 	[1, "Umar", 60],
// 	[2, "Rafi", 100],
// ];
// dataSiswa.sort(function (a, b) {
// 	if (a[2] - b[2] === 0) {
// 		return a[0] - b[0];
// 	}

// 	return a[2] - b[2];
// });
// console.log(dataSiswa);

// console.log(
// 	dataSiswa.filter(function (value) {
// 		return value[2] < 70;
// 	}),
// );
