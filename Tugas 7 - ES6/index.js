// const person = {
// 	id: 1,
// 	name: "hilmy",
// };

// // person = {
// // 	id: 2,
// // 	name: "ahmad",
// // };

// person.name = "ahmad";

// console.log(person);

// // Arrow Functions
// function jumlahkanBiasa(a, b) {
// 	return a + b;
// }

// const jumlahkanArrow = (a, b) => {
// 	return a + b;
// };

// const jumlahkanArrowShothand = (a, b) => a + b;

// console.log(jumlahkanBiasa(7, 10));
// console.log(jumlahkanArrow(7, 10));
// console.log(jumlahkanArrowShothand(7, 10));

// const createPerson = (name, age) => {
// 	return {
// 		name,
// 		age,
// 	};
// };

// const createPersonShorthand = (name, age, skinTone) => ({
// 	name,
// 	age,
// 	warnaKulit: skinTone,
// });

// console.log(createPerson("hilmy", 25));
// console.log(createPersonShorthand("hilmy", 25, "brown"));

// const member = ["hilmy", "firdaus", "muhammad"];
// member.forEach((value) => {
// 	console.log(value);
// });

// const arr1 = [0, 1, 2, 3];
// const arr2 = [4, 5, 6, 7];
// const arr3 = [...arr1, ...arr2];

// console.log(arr3);

// const car1 = {
// 	brand: "Ferrari",
// 	color: "red",
// };
// const car2 = {
// 	brand: "Ford",
// 	hp: 1000,
// };
// const car3 = {
// 	...car1,
// 	...car2,
// 	owner: "John",
// 	color: "white",
// };

// console.log(car3);

// const jumahkanRest = (a, b, ...params) => {
// 	return { a, b, params };
// };

// console.log(jumahkanRest(1, 2, 3, 4, 5, 6, 7));

// const createPersonObj = ({ name, age, hairColor }) => ({
// 	name,
// 	age,
// 	hairColor,
// });

// const person = createPersonObj({
// 	age: 10,
// 	hairColor: "black",
// 	name: "Wick",
// });

// const cat = {
// 	age: 10,
// 	hairColor: "Orange",
// 	name: "Momo",
// 	owner: "Naruto",
// 	clan: "Uzumaki",
// };

// console.log(person);

// const { age, hairColor, name } = person;
// const { age: catAge, hairColor: catHairColor, name: catName } = cat;

// console.log(age);
// console.log(person.age);

// console.log(catAge);
// console.log(cat.age);

// const { name, ...catProps } = cat;

// console.log(name);
// console.log(catProps);

// const hobbies = [
// 	"reading",
// 	"eating",
// 	"sleeping",
// 	"traveling",
// 	"hiking",
// 	"walking",
// ];

// const [elemen1, , elemen3, ...restHobbies] = hobbies;

// console.log(elemen1);
// console.log(elemen3);
// console.log(restHobbies);

// const name = "John";
// const age = 19;
// const clan = "uchiha";

// const ninja = {
// 	name: name,
// 	age: age,
// 	clan: clan,
// };

// const ninja = {
// 	name,
// 	age,
// 	clan,
// 	chidori: () => console.log("Using chidori"),
// };
// console.log(ninja);
// console.log(ninja.chidori());

// const name = "John";
// const age = 19;
// const clan = "uchiha";

// const greetings = `Hello my name is ${name} form ${clan}`;
// console.log(greetings);
