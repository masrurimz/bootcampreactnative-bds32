// var key = "color";
// var value = "red";

// var key2 = "1. Spoiler";
// var value2 = "RHRC";

// var car = {
// 	brand: "Ferrari",
// 	price: 5000,
// 	["horse power"]: 500,
// [key]: value,
// ["100 " + key]: value + value2,
// openDoor: function () {
// 	console.log("Car Door with color " + this.color + " opened");
// },
// greetings: function (name) {
// 	console.log(
// 		"Hi,",
// 		name,
// 		"I have a",
// 		this.brand,
// 		"with power",
// 		this["horse power"],
// 		"horse power",
// 	);
// },
// };

// console.log(car);
// car.openDoor();
// car.greetings("John");

// car.brand = "Ford";
// car.greetings("John");

// car["horse power"] = 1000;
// car.owner = {
// 	firstName: "John",
// 	lastName: "Wick",
// };
// car[key2] = value2;
// car.bodyKit = ["rocketBunny", "pandem", "bbk", "rwb"];

// console.log(car);

// console.log("color", car.color);
// console.log("horse power", car["horse power"]);
// console.log(car.owner.firstname);

// var input = [
// 	["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
// 	["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
// 	["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
// 	["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"],
// ];

// function dataHandling(arr = []) {
// 	for (var i = 0; i < arr.length; i++) {
// 		const element = arr[i];
// 		// console.log(element);

// 		var ttl = element[2] + " " + element[3];
// 		// var ttl = arr[i][2] + " " + arr[i][3];
// 		console.log(ttl);
// 	}
// }

// dataHandling(input);

// var outputSoal1 = {
// 	"1. Bruce Banner": {
// 		firstName: "Bruce",
// 		lastName: "Banner",
// 		gender: "male",
// 		age: 45,
// 	},
// 	"2. Natasha Romanoff": {
// 		firstName: "Natasha",
// 		lastName: "Romanoff",
// 		gender: "female",
// 		age: "Invalid Birth Year",
// 	},
// };

// console.log(outputSoal1);

// var people = [
// 	["Bruce", "Banner", "male", 1975],
// 	["Natasha", "Romanoff", "female"],
// ];

// var now = new Date();
// var thisYear = now.getFullYear(); // 2022 (tahun sekarang)

// var age = thisYear - people[1][3];

// console.log(age);

// Format Data [absen, nama, nilai]
// var studentData = [
// 	[2, "John Duro", 60],
// 	[4, "Robin Ackerman", 100],
// 	[1, "Jaeger Marimo", 60],
// 	[6, "Zoro", 80],
// 	[5, "Zenitsu", 80],
// 	[3, "Patrick Zala", 90],
// ];
// var studentData = [
// 	{ id: 2, name: "John Duro", grade: 60 },
// 	{ id: 4, name: "Robin Ackerman", grade: 100 },
// 	{ id: 1, name: "Jaeger Marimo", grade: 60 },
// 	{ id: 6, name: "Zoro", grade: 80 },
// 	{ id: 5, name: "Zenitsu", grade: 80 },
// 	{ id: 3, name: "Patrick Zala", grade: 90 },
// ];

// function sortGrade(arr) {
// 	arr.sort(function (a, b) {
// 		return a.id - b.id;
// 	});

// 	return arr;
// }

// var sortedData = sortGrade(studentData);
// console.log(sortedData);
// Output
// [
// 	[4, "Robin Ackerman", 100],
// 	[3, "Patrick Zala", 90],
// 	[5, "Zenitsu", 80],
// 	[6, "Zoro", 80],
// 	[1, "Jaeger Marimo", 60],
// 	[2, "John Duro", 60],
// ];
